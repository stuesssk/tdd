#!/usr/bin/env python3
import unittest
import TDD.exercise1.exercise1 as exercise1

class TestExercise1(unittest.TestCase):

    def test_maximum(self):
        result = exercise1.maximum(13,51)
        self.assertIsNotNone(result)

if __name__ == "__main__":
    unittest.main()
